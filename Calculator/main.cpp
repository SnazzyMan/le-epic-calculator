#include "main.h"
#include <iostream>
#include <windows.h>

double getInput()
{
	double num{};
	std::cin >> num;
	return num;
}

char getOperator()
{
	std::cout << "Input an operator.\n";
	char op{};
	std::cin >> op;
	return op;
}

double calculate(double x, double y, char op)
{
	switch (op)
	{ 
	case '+': return x + y;
		break;
	case '-': return x - y;
		break;
	case '*': return x * y;
		break;
	case '/': return x / y;
		break;
	}	
}

bool checks(double firstInput, double secondInput, char op)
{
	//divide by zero
	if (op == '/' && secondInput == 0)
	{
		std::cout << "Cannot divide by 0, non real number.\n";
		return true;
	}
	return false;
}

int repeat()
{
	std::cout << "Would you like to solve another problem? Please enter 'y' if you want to, or enter 'n' if you don't.\n";
	char answer{};
	std::cin >> answer;
	switch (answer)
	{
	case 'y': takeProblem();
		break;
	}
	return 0;
}

void takeProblem()
{
	std::cout << "Input your first number.\n";
	double firstInput{ getInput() };
	char op{ getOperator() };
	std::cout << "Input your second number.\n";
	double secondInput{ getInput() };
	if (checks(firstInput, secondInput, op) == false)
	{
		std::cout << "The answer is: " << calculate(firstInput, secondInput, op) << '\n';
	}
	repeat();
}

int main()
{
	takeProblem();
	return 0;
}